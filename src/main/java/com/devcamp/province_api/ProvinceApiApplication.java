package com.devcamp.province_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceApiApplication.class, args);
	}

}
