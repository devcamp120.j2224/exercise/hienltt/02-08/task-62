package com.devcamp.province_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_api.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Integer>{
    
}
