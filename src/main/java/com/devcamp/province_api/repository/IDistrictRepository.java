package com.devcamp.province_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_api.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer>{
    CDistrict findByDistrictId(int districtId);
}
