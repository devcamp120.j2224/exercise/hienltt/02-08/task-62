package com.devcamp.province_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_api.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer>{
    CProvince findByProvinceId(int provinceId);
}
