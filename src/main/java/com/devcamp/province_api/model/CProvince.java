package com.devcamp.province_api.model;

import java.util.Set;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "province")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int provinceId;
    
    @Column(name = "code")
    private String code;
    
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "province", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CDistrict> districts;

    public CProvince() {
    }

    public CProvince(int provinceId, String code, String name, Set<CDistrict> districts) {
        this.provinceId = provinceId;
        this.code = code;
        this.name = name;
        this.districts = districts;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }
}
