package com.devcamp.province_api.model;

import java.util.Set;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "district")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int districtId;

    @Column(name = "name")
    private String name;
    
    @Column(name = "prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "provinceId")
    @JsonBackReference
    private CProvince province;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CWard> wards;

    public CDistrict() {
    }

    public CDistrict(int districtId, String name, String prefix, CProvince province, Set<CWard> wards) {
        this.districtId = districtId;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }
}
