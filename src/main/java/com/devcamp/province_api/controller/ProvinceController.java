package com.devcamp.province_api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.province_api.model.*;
import com.devcamp.province_api.repository.*;

@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    IProvinceRepository iProvinceRepository;
    @Autowired
    IDistrictRepository iDistrictRepository;

    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getProvinceList(){
        try {
            List<CProvince> listProvince = new ArrayList<>();
            iProvinceRepository.findAll().forEach(listProvince :: add);
            return new ResponseEntity<List<CProvince>>(listProvince, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceId(@RequestParam(value = "provinceId", required = true) int provinceId){
        try {
            CProvince province = iProvinceRepository.findByProvinceId(provinceId);
            if(province != null){
                return new ResponseEntity<Set<CDistrict>>(province.getDistricts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards")
    public ResponseEntity<Set<CWard>> getWardByDistrictId(@RequestParam(value = "districtId", required = true) int districtId){
        try {
            CDistrict district = iDistrictRepository.findByDistrictId(districtId);
            if(district != null){
                return new ResponseEntity<Set<CWard>>(district.getWards(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
